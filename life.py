When you're feeling in the dumps,
Don't be silly chumps,
Just purse your lips and whistle -- that's the thing!

Other things just make you swear and curse.
When you're chewing on life's gristle,
Don't grumble, give a whistle!

If life seems jolly rotten,
There's something you've forgotten!
And that's to laugh and smile and dance and sing,

question_list = [When youre feeling in the dumps, If life seems jolly rotten, When youre chewing on lifes gristle,]

answers_list = [Just purse your lips and whistle -- thats the thing!, Dont grumble, give a whistle!, And thats to laugh and smile and dance and sing,]

input(questions[When youre feeling in the dumps])
print(answers[Just purse your lips and whistle])

input(questions[If life seems jolly rotten])
print(answers[Dont grumble, give a whistle!])

input(questions[When youre chewing on lifes gristle])
print(answers[And thats to laugh and smile and dance and sing])
